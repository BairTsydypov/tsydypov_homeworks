package com.company;
public class Main {


    public static int getDigitPosition(int number, int[] array2) {

        int pos1 = -1;

        for (int i = 0; i < array2.length; i++) {
            if (array2[i] == number) {pos1 = i;}
        }
        return pos1;      }

    public static void print(int[] array) {
       int zerocount = 0;
        for (int i = 0; i < array.length; i++) {
           if (array[i]!= 0)
           {System.out.print(array[i] + " ");}
           else {zerocount++;}
        }
        for (int i = 0; i < zerocount; i++) {
            System.out.print(0 + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {

        int[] a = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        int result = getDigitPosition(14, a);

        System.out.println(result);
        print(a);
    }
}
