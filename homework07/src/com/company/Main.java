package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

    /**
     * На вход подается последовательность чисел, оканчивающихся на -1.
     * Необходимо вывести число, которе присутствует в последовательности минимальное количество раз.
     * Гарантируется:
     * Все числа в диапазоне от -100 до 100.
     * Числа встречаются не более 2 147 483 647-раз каждое.
     * Сложность алгоритма - O(n)
     */

    public class Main {
        public static void main(String[] args) {
            int[] array = getArray();
            System.out.println("Минимальное количество повторений в массиве у числа: " + getMinOfSeries(array));
        }
        /**
         * метод запрашивает у пользователя вводить числа от -100 до 100,
         * до тех пор пока не будет введена цифра -1, или максимальное количество
         * и возвращает массив введёных элементов;
         *
         * @return возвращает массив чисел (числы в пределе от -100 до 100);
         */
        static int[] getArray() {
            List<Integer> list = new ArrayList<>();

            System.out.println("Вводите числа от -100 до 100. Ввод прекратится, если Вы введёте -1");
            Scanner scanner = new Scanner(System.in);
            int inValue = scanner.nextInt();

            while (inValue != -1) {
                if (inValue > -101 && inValue < 101)
                    list.add(inValue);
                else
                    System.out.println("Вводите числа от -100 до 100");
                inValue = scanner.nextInt();
            }
            int[] array = new int[list.size()];

            for (int i = 0; i < array.length; i++) {
                array[i] = list.get(i);
            }
            return array;
        }
        /**
         * метод возвращает число, которое меньшее количество раз повторяется;
         *
         * @param arrayInput - массив, в котором должны содержаться числа в диапазоне -100 до 100, цифры -1 нет;
         * @return - первое число, которое имеет меньше повторений в массиве array;
         */
        static int getMinOfSeries(int[] arrayInput) {
            if (arrayInput.length == 0)
                throw new RuntimeException("массив пустой");

            int[] arrayOfNumOfDigits = getArrayOfNumOfDigits(arrayInput);

            //находим минимальную последовательность
            int min = 2147483647;
            int value = -100;
            for (int i = 0; i < arrayOfNumOfDigits.length; i++) {
                if (arrayOfNumOfDigits[i] != 0) {
                    if (arrayOfNumOfDigits[i] < min) {
                        min = arrayOfNumOfDigits[i];
                        value = i - 100;
                    }
                }
            }
            return value;
        }


        /**
         * формируем массив количества чисел , которые встречаются в переданном массиве,
         * т.е. в [0] находится количество числа -100, а в [201] количество числа 100
         * @param arrayInput - входной массив
         * @return возвращаем массив количества чисел
         */
        static int[] getArrayOfNumOfDigits (int [] arrayInput){
            int[] arrayOfNumOfDigits = new int[201];
            for (int i = 0; i < arrayInput.length; i++) {
                if (arrayInput[i] > -101 && arrayInput[i] < 101) {
                    arrayOfNumOfDigits[arrayInput[i] + 100]++;
                }
            }
            return arrayOfNumOfDigits;
        }


    }
